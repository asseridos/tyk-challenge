class Image < ApplicationRecord
  mount_uploader :photo, PhotoUploader
  default_scope { order(created_at: :desc) }

  validate :validate_maximum_photo_dimensions, on: :create

  attr_accessor :width, :height, :size
  attr_accessor :photo_cache

  private

  def validate_maximum_photo_dimensions
    errors.add :photo, 'width should be less than 1000px.' if width.present? && width > 1000
    errors.add :photo, 'height should be less than 1000px.' if height.present? && height > 1000
    errors.add :photo, 'size should be less than 250Kb.' if size.present? && size > 250000
  end
end
